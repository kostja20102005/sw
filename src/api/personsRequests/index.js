import { starWarsCharacters } from "../persons/index";

export const fakeBackend = {
    starWars: {
      getAllPersons: () => {
        return new Promise((resolve) => {
          if (starWarsCharacters === undefined) {
            setTimeout(() => {
              resolve({
                status: 404,
                error: `Персонажи не найдены`,
              });
            }, 1000);
          } else {
            return setTimeout(() => {
              resolve({
                status: 200,
                message: "Успешно",
                starWars: starWarsCharacters
              }); 
            }, 1000);
          }
        });
      },
      findPersonById: (id) => {
        const person = starWarsCharacters.find((person) => {
            return person.id === id
        })
        return new Promise((resolve) => {
          if (person === undefined) {
            setTimeout(() => {
              resolve({
                status: 404,
                error: `Персонаж не найден`,
              });
            }, 1000);
          } else {
            return setTimeout(() => {
              resolve({
                status: 200,
                message: "Успешно",
                person,
              }); 
            }, 1000);
          }
        });
      },
    },
  };