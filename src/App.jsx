import './App.css';
import { Routes, Route } from "react-router-dom";
import PersonProfile from './pages/PersonProfile';

function App() {
  return (
    <Routes>
      <Route path='/'>
        <Route path=':id' element={<PersonProfile />} />
      </Route>
    </Routes>
  );
}

export default App;
