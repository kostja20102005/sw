import React from 'react'
import { useParams } from "react-router-dom";
import { fakeBackend } from '../../api/personsRequests';

export default function PersonProfile() {
    const [person, setPerson] = React.useState({});
    let { id } = useParams();

    React.useEffect(() => { 
        async function fetchPerson() {
            const foundPerson = await fakeBackend.starWars.findPersonById(Number(id));
            setPerson(foundPerson.person)
          }
        fetchPerson()
      }, [id])
  return (
    <div>{person.name}</div>
  )
}